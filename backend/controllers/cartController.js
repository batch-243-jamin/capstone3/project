// import modules
const Product = require('../models/Product');
const User = require('../models/User');
const Cart = require('../models/Cart');
const auth = require('../auth');

// Add to cart
const addToCart = async (request, response) => {
	console.log(request.body);

	// get User payload
	const userData = auth.decode(request.headers.authorization);

	// get target Product details
	const productId = request.params.productId;

	const cart = await Cart.findOne({productId, userId: userData.id});
	console.log("@USERCART", cart);

	const product = await Product.findById(productId);
	if(!cart) {
	
		if (!userData.isAdmin) {
			
			let productAddedToCart;

			let newCart = new Cart(
				{
					userId: userData.id,
					productId,
					title: product.title,
					quantity: request.body.quantity,
					amount: product.price * request.body.quantity
				}
			);
			
			// to Cart model
			await newCart.save();
			console.log("@NEWCART", newCart);
	
			// update user cart
			const user = await User.findById(userData.id);
			user.cart.push(newCart);
			await user.save();
	
			return response.send({productAddedToCart: true});
		} else {
			response.send({isUserAdmin: true});
		}
	} else {
		cart.quantity += request.body.quantity
		let amount = product.price * request.body.quantity
		cart.amount += amount
		cart.save()
		response.json(cart);
	}
}

// Remove from cart
const removeFromCart = async (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const cartItemId = request.params.cartItemId;
	const cart = await Cart.find({id: userData.id})
	
	if(cart) {
		const cartItem = await Cart.findOneAndDelete({_id: cartItemId})
		if(!cartItem) {
			return response.status(400).json({error: 'No items found.'})
		}
		
		// update user cart
        const user = await User.findById(userData.id);
		let items = user.cart;

		const updatedCart = items.filter(item =>{
			if(!item.equals(cartItem._id)) {
				return item;
			}
		})

		user.cart = updatedCart;
		await user.save();
		
		response.status(200).json(cartItem);
	}
}

// get Authenticated User cart
const getUserCart = async (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	console.log("@USERDATA",  userData);

	if(!userData.isAdmin) {
		try {

			const result = await Cart.aggregate([
				{$match : {
				userId : userData.id
				}},

			])

			console.log("@1", result);
			if(result) {
				return response.send(result);
			}
		} catch (error) {
			console.log("ERROR@", error);
		}
		
	} else {
		let isUserAdmin;
		response.send({isUserAdmin: true});	
	}
}

module.exports = {
    addToCart,
	removeFromCart,
	getUserCart,
}
const Product = require("../models/Product");
const auth =require("../auth");

module.exports.addProduct = async (request, response) =>{
		
		let token = request.headers.authorization;
		const userData = auth.decode(token);
		console.log(userData);
		if(userData.isAdmin){
			let newProduct = new Product({
				title: request.body.title,
				description: request.body.description,
				price: request.body.price,
				stock: request.body.stock
			})

			let productCreated;
			await newProduct.save()

			response.send({productCreated: true})
			// .then(result => {
			// 	console.log(result);
			//  response.send(true);
			// }).catch(error => {
			// 	console.log(error);
			//  response.send(false);
			// })
		}
		else{
			response.send("You do not have an Admin role!")
		}
}

// Retrieve all active products

module.exports.getAllActive = (request, response) =>{
	return Product.find({isActive: true})
	.then(result => {
		response.send(result);
	}).catch(err => {
		response.send(err);
	})
}

// Retrive all products 

module.exports.getAllProducts = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin) {
        let isUserAdmin;
		return response.send({ isUserAdmin: false });
	} else {
        let allProductsRender;
		return Product.find({})
		.then(result => response.send(result))
		.catch(err => {
			console.log(err);
			response.send(err);
		})
	}
}


// retrieving a specific product



// Retrieve Single Product
module.exports.getSingleProduct = (request, response) => {
	const productId = request.params.productId;

	return Product.findById(productId).then(result => {
		console.log(`Rendering ${result.title}`)
		response.json(result);
	}).catch(err => {
		response.json(err);
	})
}

// update a product

module.exports.updateProduct = (request, response) => {
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	console.log(userData);

		let updatedProduct = {
			title: request.body.title,
			description: request.body.description,
			price: request.body.price,
			stocks: request.body.stocks
		}

	const productId = request.params.productId;

	if(userData.isAdmin){
		return Product.findByIdAndUpdate(productId, updatedProduct, {new:true}).then(result => {
			response.send(result)
		}).catch(err =>{
			response.send(err);
		})
	}else{
		return response.send("You don't have access to this page!");
	}
}

// archive a product

module.exports.archiveProduct = (request, response) => {
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	let productId = request.params.productId;
	let updateIsActive = {
			isActive: request.body.isActive
		}
	

	if(userData.isAdmin){
		return Product.findByIdAndUpdate(productId, updateIsActive, {new:true}).then(result => {
			response.send(true)
		}).catch(error =>{
			response.send(false);
		})
	}
	else{
		return response.send("You don't have access to this page!");
	}

}

// unarchive a product

module.exports.unArchiveProduct = (request, response) => {
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	let productId = request.params.productId;
	let updateIsActive = {
			isActive: request.body.isActive
		}
	

	if(userData.isAdmin){
		return Product.findByIdAndUpdate(productId, updateIsActive, {new:true}).then(result => {
			response.send(true)
		}).catch(error =>{
			response.send(false);
		})
	}
	else{
		return response.send("You don't have access to this page!");
	}

}
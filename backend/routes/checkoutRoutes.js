// dependencies
const express = require("express");
const router = express.Router();

// modules
const checkoutController = require('../controllers/checkoutController');
const auth = require('../auth');

// checkout
router.post("/checkout/:cartItemId", auth.verify, checkoutController.checkout);

module.exports = router;
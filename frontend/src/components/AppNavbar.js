// importing modules using deconstruct
import {Container, Nav, Navbar} from 'react-bootstrap';
import {Fragment, useContext} from 'react'
import {NavLink} from "react-router-dom";
import userContext from '../UserContext';

export default function AppNavbar(){

  const { user } = useContext(userContext);

	return(
		 <Navbar bg="light" expand="lg" className="vw-100">
      <Container fluid>
        <Navbar.Brand as = {NavLink} to = "/">LogoExample</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            {
              (user.id !== null) 
              ? (user.isAdmin === true)
              ? <>
                <Nav.Link as = {NavLink} to = "/dashboard">Dashboard</Nav.Link>
                <Nav.Link as = {NavLink} to = "/allProducts">All Products</Nav.Link>
                <Nav.Link as = {NavLink} to = "/add">Create Product</Nav.Link>
                <Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
                </>
              : <>
                <Nav.Link as = {NavLink} to = "/">Home</Nav.Link>
                <Nav.Link as = {NavLink} to = "/products">Products</Nav.Link>
                <Nav.Link as = {NavLink} to = "/cart">Cart</Nav.Link>
                <Nav.Link as = {NavLink} to = "/logout">Logout</Nav.Link>
                </>
              : <>
                <Nav.Link as = {NavLink} to = "/">Home</Nav.Link>
                <Nav.Link as = {NavLink} to = "/products">Products</Nav.Link>
                <Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>
                <Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>
                </>
            }
            
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
		)
}
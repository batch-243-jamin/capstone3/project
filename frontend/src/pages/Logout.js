import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import {useContext, useEffect} from 'react';
import Swal from 'sweetalert2';

export default function Logout(){

	const {user, setUser, unSetUser} = useContext(UserContext);
	console.log(user);
	unSetUser();

	useEffect(()=>{
		setUser({id: null, isAdmin: false});
	}, [])

	Swal.fire({
		title: "Successfully logged out!",
		icon: "info",
		text: "Thank you and come again!"
	})

		return(
			<Navigate to = '/login'/>
		)
}
import {Container} from 'react-bootstrap';
import React from 'react';
import Footer from '../components/Footer';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(){

	return(

		<Container>
			<Banner/>
			<Highlights/>
			{/*<Footer/>*/}
		</Container>
		)
}
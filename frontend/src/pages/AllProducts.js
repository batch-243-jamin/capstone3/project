import {Fragment, useEffect, useState} from 'react';
import ProductCard from '../components/ProductCard';

export default function AllProducts(){

	const [products, setProducts] = useState([]);

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URI}/products/allProducts`, {
        headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
    })
		.then(response => response.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product => {

		return(
			<ProductCard key = {product._id} productProp = {product}/>)
		}))
	})

	}, []);

	return(
		<Fragment>
		{products}
		</Fragment>
		)
}